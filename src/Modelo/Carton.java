/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;


/**
 *
 * @author Alejandro Cortes
 */


public class Carton 
{
    
    public JPanel Pfilas[][];
    public int Pcolumnas[][];
    public ArrayList Pnumeros;
    public JPanel Pfilas1[][];
    public int Pcolumnas1[][];
    public ArrayList Pnumeros1;
    public JPanel Pfilas2[][];
    public int Pcolumnas2[][];
    public ArrayList Pnumeros2;
    public JPanel Pfilas3[][];
    public int Pcolumnas3[][];
    public ArrayList Pnumeros3;
    public JPanel Pfilas4[][];
    public int Pcolumnas4[][];
    public ArrayList Pnumeros4;
    public JPanel Pfilas5[][];
    public int Pcolumnas5[][];
    public ArrayList Pnumeros5;
    public JPanel TODO = new JPanel();
    public JPanel TODO1 = new JPanel();
    public JPanel TODO2 = new JPanel();
    public JPanel TODO3 = new JPanel();
    public JPanel TODO4 = new JPanel();
    public JPanel TODO5 = new JPanel();
    
 
       
    
    public Carton ()
    {
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas = new JPanel[6][5];
        Pcolumnas = new int[5][5];
        palabra(p);
        Pnumeros = new ArrayList();
        for(int i = 1; i < Pfilas.length; i++)
        {
            for(int j = 0; j < Pfilas[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas[i][j] = new JPanel();
                Pfilas[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas[i][j].add(new JLabel(" "));
                    Pfilas[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas[i - 1][j] = 0;
                } else
                {
                    Pcolumnas[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas[i][j].add(l);
                    Pfilas[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas[i][j].setEnabled(false);
                p.add(Pfilas[i][j]);
                Pnumeros.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO.add(p);
        TODO.setBackground(Color.yellow);
        
    }
    
    public JPanel segundo(){
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas1 = new JPanel[6][5];
        Pcolumnas1 = new int[5][5];
        palabra(p);
        Pnumeros1 = new ArrayList();
        for(int i = 1; i < Pfilas1.length; i++)
        {
            for(int j = 0; j < Pfilas1[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros1.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas1[i][j] = new JPanel();
                Pfilas1[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas1[i][j].add(new JLabel(" "));
                    Pfilas1[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas1[i - 1][j] = 0;
                } else
                {
                    Pcolumnas1[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas1[i][j].add(l);
                    Pfilas1[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas1[i][j].setEnabled(false);
                p.add(Pfilas1[i][j]);
                Pnumeros1.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO1.add(p);
        TODO1.setBackground(Color.yellow);
        return TODO1;
        
    }
    
    public JPanel tercero(){
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas2 = new JPanel[6][5];
        Pcolumnas2 = new int[5][5];
        palabra(p);
        Pnumeros2 = new ArrayList();
        for(int i = 1; i < Pfilas2.length; i++)
        {
            for(int j = 0; j < Pfilas2[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros2.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas2[i][j] = new JPanel();
                Pfilas2[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas2[i][j].add(new JLabel(" "));
                    Pfilas2[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas2[i - 1][j] = 0;
                } else
                {
                    Pcolumnas2[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas2[i][j].add(l);
                    Pfilas2[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas2[i][j].setEnabled(false);
                p.add(Pfilas2[i][j]);
                Pnumeros2.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO2.add(p);
        TODO2.setBackground(Color.yellow);
        return TODO2;
        
    }
    
    public JPanel cuarto(){
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas3 = new JPanel[6][5];
        Pcolumnas3 = new int[5][5];
        palabra(p);
        Pnumeros3 = new ArrayList();
        for(int i = 1; i < Pfilas3.length; i++)
        {
            for(int j = 0; j < Pfilas3[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros3.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas3[i][j] = new JPanel();
                Pfilas3[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas3[i][j].add(new JLabel(" "));
                    Pfilas3[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas3[i - 1][j] = 0;
                } else
                {
                    Pcolumnas3[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas3[i][j].add(l);
                    Pfilas3[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas3[i][j].setEnabled(false);
                p.add(Pfilas3[i][j]);
                Pnumeros3.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO3.add(p);
        TODO3.setBackground(Color.yellow);
        return TODO3;
        
    }
    
    public JPanel quinto(){
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas4 = new JPanel[6][5];
        Pcolumnas4 = new int[5][5];
        palabra(p);
        Pnumeros4 = new ArrayList();
        for(int i = 1; i < Pfilas4.length; i++)
        {
            for(int j = 0; j < Pfilas4[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros4.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas4[i][j] = new JPanel();
                Pfilas4[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas4[i][j].add(new JLabel(" "));
                    Pfilas4[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas4[i - 1][j] = 0;
                } else
                {
                    Pcolumnas4[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas4[i][j].add(l);
                    Pfilas4[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas4[i][j].setEnabled(false);
                p.add(Pfilas4[i][j]);
                Pnumeros4.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO4.add(p);
        TODO4.setBackground(Color.yellow);
        return TODO4;
        
    }
    
    public JPanel sexto(){
        JPanel p = new JPanel();
        p.setBackground(Color.blue);
        p.setLayout(new GridLayout(6, 5));
        Pfilas5 = new JPanel[6][5];
        Pcolumnas5 = new int[5][5];
        palabra(p);
        Pnumeros5 = new ArrayList();
        for(int i = 1; i < Pfilas5.length; i++)
        {
            for(int j = 0; j < Pfilas5[0].length; j++)
            {
                int n;
                for(n = 15 * j + (int)(Math.random() * 15D) + 1; Pnumeros5.contains(Integer.valueOf(n)); n = 15 * j + (int)(Math.random() * 15D) + 1);
                Pfilas5[i][j] = new JPanel();
                Pfilas5[i][j].setLayout(new BorderLayout());
                if(i == 3 && j == 2)
                {
                    Pfilas5[i][j].add(new JLabel(" "));
                    Pfilas5[i][j].setBorder(new LineBorder(Color.GREEN, 15));
                    Pcolumnas5[i - 1][j] = 0;
                } else
                {
                    Pcolumnas5[i - 1][j] = n;
                    JLabel l = new JLabel((new StringBuilder()).append(n).toString());
                    l.setHorizontalAlignment(0);
                    Pfilas5[i][j].add(l);
                    Pfilas5[i][j].setBorder(new LineBorder(Color.GREEN, 2));
                }
                Pfilas5[i][j].setEnabled(false);
                p.add(Pfilas5[i][j]);
                Pnumeros5.add(Integer.valueOf(n));
            }

        }
       

        p.setPreferredSize(new Dimension(150, 150));
        TODO5.add(p);
        TODO5.setBackground(Color.yellow);
        return TODO5;
        
    }
   
    
    

    public void palabra(JPanel p)
    {
        String v[] = {
            "B", "I", "N", "G", "O"
        };
        for(int i = 0; i < 5; i++)
        {
            Pfilas[0][i] = new JPanel();
            Pfilas[0][i].setLayout(new BorderLayout());
            Pfilas[0][i].setBackground(Color.green);
            JLabel b = new JLabel(v[i]);
            b.setHorizontalAlignment(0);
            Pfilas[0][i].setBorder(new LineBorder(Color.green, 1));
            Pfilas[0][i].add(b);
            p.add(Pfilas[0][i]);
        }

    }
    
    
      

   
    public JPanel getTODO() {
        
        return TODO;
    }

    public JPanel[][] getPfilas() {
        return Pfilas;
    }

    public void setPfilas(JPanel[][] Pfilas) {
        this.Pfilas = Pfilas;
    }

    public int[][] getPcolumnas() {
        return Pcolumnas;
    }

    public void setPcolumnas(int[][] Pcolumnas) {
        this.Pcolumnas = Pcolumnas;
    }

    public ArrayList getPnumeros() {
        return Pnumeros;
    }

    public void setPnumeros(ArrayList Pnumeros) {
        this.Pnumeros = Pnumeros;
    }

    public JPanel[][] getPfilas1() {
        return Pfilas1;
    }

    public void setPfilas1(JPanel[][] Pfilas1) {
        this.Pfilas1 = Pfilas1;
    }

    public int[][] getPcolumnas1() {
        return Pcolumnas1;
    }

    public void setPcolumnas1(int[][] Pcolumnas1) {
        this.Pcolumnas1 = Pcolumnas1;
    }

    public ArrayList getPnumeros1() {
        return Pnumeros1;
    }

    public void setPnumeros1(ArrayList Pnumeros1) {
        this.Pnumeros1 = Pnumeros1;
    }

    public JPanel[][] getPfilas2() {
        return Pfilas2;
    }

    public void setPfilas2(JPanel[][] Pfilas2) {
        this.Pfilas2 = Pfilas2;
    }

    public int[][] getPcolumnas2() {
        return Pcolumnas2;
    }

    public void setPcolumnas2(int[][] Pcolumnas2) {
        this.Pcolumnas2 = Pcolumnas2;
    }

    public ArrayList getPnumeros2() {
        return Pnumeros2;
    }

    public void setPnumeros2(ArrayList Pnumeros2) {
        this.Pnumeros2 = Pnumeros2;
    }

    public JPanel[][] getPfilas3() {
        return Pfilas3;
    }

    public void setPfilas3(JPanel[][] Pfilas3) {
        this.Pfilas3 = Pfilas3;
    }

    public int[][] getPcolumnas3() {
        return Pcolumnas3;
    }

    public void setPcolumnas3(int[][] Pcolumnas3) {
        this.Pcolumnas3 = Pcolumnas3;
    }

    public ArrayList getPnumeros3() {
        return Pnumeros3;
    }

    public void setPnumeros3(ArrayList Pnumeros3) {
        this.Pnumeros3 = Pnumeros3;
    }

    public JPanel[][] getPfilas4() {
        return Pfilas4;
    }

    public void setPfilas4(JPanel[][] Pfilas4) {
        this.Pfilas4 = Pfilas4;
    }

    public int[][] getPcolumnas4() {
        return Pcolumnas4;
    }

    public void setPcolumnas4(int[][] Pcolumnas4) {
        this.Pcolumnas4 = Pcolumnas4;
    }

    public ArrayList getPnumeros4() {
        return Pnumeros4;
    }

    public void setPnumeros4(ArrayList Pnumeros4) {
        this.Pnumeros4 = Pnumeros4;
    }

    public JPanel[][] getPfilas5() {
        return Pfilas5;
    }

    public void setPfilas5(JPanel[][] Pfilas5) {
        this.Pfilas5 = Pfilas5;
    }

    public int[][] getPcolumnas5() {
        return Pcolumnas5;
    }

    public void setPcolumnas5(int[][] Pcolumnas5) {
        this.Pcolumnas5 = Pcolumnas5;
    }

    public ArrayList getPnumeros5() {
        return Pnumeros5;
    }

    public void setPnumeros5(ArrayList Pnumeros5) {
        this.Pnumeros5 = Pnumeros5;
    }

    public JPanel getTODO1() {
        return TODO1;
    }

    public void setTODO1(JPanel TODO1) {
        this.TODO1 = TODO1;
    }

    public JPanel getTODO2() {
        return TODO2;
    }

    public void setTODO2(JPanel TODO2) {
        this.TODO2 = TODO2;
    }

    public JPanel getTODO3() {
        return TODO3;
    }

    public void setTODO3(JPanel TODO3) {
        this.TODO3 = TODO3;
    }

    public JPanel getTODO4() {
        return TODO4;
    }

    public void setTODO4(JPanel TODO4) {
        this.TODO4 = TODO4;
    }

    public JPanel getTODO5() {
        return TODO5;
    }

    public void setTODO5(JPanel TODO5) {
        this.TODO5 = TODO5;
    }
    
    
    
    

   

    
}