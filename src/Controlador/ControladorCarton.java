/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import Modelo.Carton;
import Modelo.TablaCarton;
import Vista.Ganador;
import Vista.Principal;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;





/**
 *
 * @author Alejandro Cortes
 */
public class ControladorCarton implements ActionListener {
    Principal objetoVista;
    TablaCarton objetoH;
    Carton H;
    

    public ControladorCarton(Principal objetoVista, TablaCarton objetoH, Carton H) {
        this.objetoVista = objetoVista;
        objetoVista.getjButton1().addActionListener(this);
        this.objetoH = objetoH;
        objetoH.getBntAleatorio().addActionListener(this);
        this.H = H;
      
        
    }
    
    public ControladorCarton() {
        this.objetoVista = new Principal();
        objetoVista.getjButton1().addActionListener(this);
        this.objetoH = new TablaCarton();
        objetoH.getBntAleatorio().addActionListener(this);
        this.H = new Carton();
        
        
    }
   
    public void iniciar(){
        
        objetoVista.setTitle("BINGO");
        objetoVista.setVisible(true);
        
        
    }
    
    
    
    
    public void actionPerformed(ActionEvent e) {
         
        if(e.getSource()==objetoVista.getjButton1()){
            if(objetoVista.getjCheckBox1().isSelected() || objetoVista.getjCheckBox2().isSelected() || objetoVista.getjCheckBox3().isSelected() || objetoVista.getjCheckBox4().isSelected()){
                DefaultTableModel  modelo;
            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setSize(900,900);
            JPanel TODO = new JPanel();
            frame.add(TODO);
            TODO.setSize(800,800);
            TODO.setLayout(new GridLayout(2, 0));
            JPanel T = new JPanel();
            T.setBackground(Color.yellow);
            T.setSize(600,600);
            
            JPanel p = new JPanel();
            p.add(objetoH.getTODO());
            
            T.add(p);
                    T.setVisible(true);
                    
            
            TODO.add(T);
            JPanel S = new JPanel();
            JPanel t = new JPanel();
            S.setBackground(Color.red);
            
            
            int x;

            x= objetoVista.getjComboBox1().getSelectedIndex()+1;
               
                    if(x==1){
                        S.add(H.getTODO());
                    }else{
                        if(x==2){
                            S.add(H.getTODO());
                            S.add(H.segundo());
                        }else{
                            if(x==3){
                                S.add(H.getTODO());
                                S.add(H.segundo());
                                S.add(H.tercero());
                            }else{
                                if(x==4){
                                    S.add(H.getTODO());
                                    S.add(H.segundo());
                                    S.add(H.tercero());
                                    S.add(H.cuarto());
                                }else{
                                    if(x==5){
                                        S.add(H.getTODO());
                                        S.add(H.segundo());
                                        S.add(H.tercero());
                                        S.add(H.cuarto());
                                        S.add(H.quinto());
                                    }else{
                                        if(x==6){
                                            S.add(H.getTODO());
                                            S.add(H.segundo());
                                            S.add(H.tercero());
                                            S.add(H.cuarto());
                                            S.add(H.quinto());
                                            S.add(H.sexto());
                                        }
                                    }
                                }
                            }
                        }
                    }
                        
                   
                    
                    
                    
                    
                
            S.setSize(500,500);
            TODO.add(S);
            
            
           
          
            
             
                    
            objetoH.getBntAleatorio().addActionListener(new ActionListener() {
                List<Integer> bal = new ArrayList();
                int contbal = 0;
                int nb = 75;
              
                
                
                
                @Override
                @SuppressWarnings("empty-statement")
            public void actionPerformed(ActionEvent arg0)       
            {
                
                
                if(contbal <nb)
                {
                    Random n1= new Random();
                    
                    for(valor = (int)(Math.random() * 75D) + 1; bal.contains(valor); valor = (int)(Math.random() * 75D) + 1);
             
                    objetoH.getLbl1().setText(""+valor);
                    String datos = objetoH.getTxt1().getText();
                 
                    if(valor<=15&&valor>=1){
                        objetoH.getLbl1().setText( "B "+valor);
                        bal.add(valor);
                        contbal++;
                    objetoH.getTxt1().setText("Balota sorteada # " + contbal);
                    }
                    if(valor<=30&&valor>=16){
                       objetoH.getLbl1().setText( "I "+valor);
                        bal.add(valor);
                        contbal++;
                    objetoH.getTxt1().setText("Balota sorteada # " + contbal);
                    }
                    if(valor<=45&&valor>=31){
                        objetoH.getLbl1().setText( "N "+valor);
                        bal.add(valor);
                        contbal++;
                    objetoH.getTxt1().setText("Balota sorteada # " + contbal);
                    }
                    if(valor<=60&&valor>=46){
                        objetoH.getLbl1().setText( "G "+valor);
                        bal.add(valor);
                        contbal++;
                    objetoH.getTxt1().setText("Balota sorteada # " + contbal);
                    }
                    if(valor<=75&&valor>=61){
                        objetoH.getLbl1().setText( "O "+valor);
                        bal.add(valor);
                        contbal++;
                    objetoH.getTxt1().setText("Balota sorteada # " + contbal);
                    }
                    
                     for(int i = 0; i < objetoH.columnas.length; i++)
                     {
                     for(int j = 0; j < objetoH.columnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == objetoH.columnas[i][j]){
                    objetoH.filas[i+1][j].setBackground(Color.ORANGE);
                      }
                
            }
                    
        }
                     if(objetoVista.getjCheckBox1().isSelected()){
                         if(x==1){
                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                      
                       cont1 ++;
                       
                       if(cont1 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                      
                
            }

        }
                     }else{
                         if(x==2){
                              for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                          
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                     cont1 ++;
                       
                       if(cont1 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");                       }
                      
                      
                      }
                
            }

        }
                              
                              for(int s = 0; s < H.getPcolumnas1().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas1()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas1()[s][j]){
                      H.getPfilas1()[s+1][j].setBackground(Color.cyan);
                      cont2 ++;
                       
                       if(cont2 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                
            }

        }
                         }else{
                             if(x==3){
                                 for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                      cont1 ++;
                       
                       if(cont1 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                
            }

        }
                              for(int s = 0; s < H.getPcolumnas1().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas1()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas1()[s][j]){
                      H.getPfilas1()[s+1][j].setBackground(Color.cyan);
                      cont2 ++;
                       
                       if(cont2 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas2().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas2()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas2()[s][j]){
                      H.getPfilas2()[s+1][j].setBackground(Color.cyan);
                      cont3 ++;
                       
                       if(cont3 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                
            }

        }
                             }else{
                                 if(x==4){
                                     for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                      cont1 ++;
                       
                       if(cont1 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                
            }

        }
                              for(int s = 0; s < H.getPcolumnas1().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas1()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas1()[s][j]){
                      H.getPfilas1()[s+1][j].setBackground(Color.cyan);
                      cont2 ++;
                       
                       if(cont2 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas2().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas2()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas2()[s][j]){
                      H.getPfilas2()[s+1][j].setBackground(Color.cyan);
                      cont3 ++;
                       
                       if(cont3 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas3().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas3()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas3()[s][j]){
                      H.getPfilas3()[s+1][j].setBackground(Color.cyan);
                      cont4 ++;
                       
                       if(cont4 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                
            }

        }
                                 
                                 }else{
                                     if(x==5){
                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                      cont1 ++;
                       
                       if(cont1 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                
            }

        }
                              for(int s = 0; s < H.getPcolumnas1().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas1()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas1()[s][j]){
                      H.getPfilas1()[s+1][j].setBackground(Color.cyan);
                      cont2 ++;
                       
                       if(cont2 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas2().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas2()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas2()[s][j]){
                      H.getPfilas2()[s+1][j].setBackground(Color.cyan);
                      cont3 ++;
                       
                       if(cont3 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas3().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas3()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas3()[s][j]){
                      H.getPfilas3()[s+1][j].setBackground(Color.cyan);
                      cont4 ++;
                       
                       if(cont4 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas4().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas4()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas4()[s][j]){
                      H.getPfilas4()[s+1][j].setBackground(Color.cyan);
                      cont5 ++;
                       
                       if(cont5 ==24){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                
            }

        }
                                 
                                     }else{
                                         if(x==6){
                                             for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.Pcolumnas[s][j]){
                      H.Pfilas[s+1][j].setBackground(Color.cyan);
                      cont1 ++;
                       
                       if(cont1 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                
            }

        }
                              for(int s = 0; s < H.getPcolumnas1().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas1()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas1()[s][j]){
                      H.getPfilas1()[s+1][j].setBackground(Color.cyan);
                      cont2 ++;
                       
                       if(cont2 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas2().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas2()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas2()[s][j]){
                      H.getPfilas2()[s+1][j].setBackground(Color.cyan);
                      cont3 ++;
                       
                       if(cont3 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas3().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas3()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas3()[s][j]){
                      H.getPfilas3()[s+1][j].setBackground(Color.cyan);
                      cont4 ++;
                       
                       if(cont4 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas4().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas4()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas4()[s][j]){
                      H.getPfilas4()[s+1][j].setBackground(Color.cyan);
                      cont5 ++;
                       
                       if(cont5 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                
            }

        }
                               for(int s = 0; s < H.getPcolumnas5().length; s++)
                     {
                     for(int j = 0; j < H.getPcolumnas5()[0].length; j++)
                     {  
                        int a = valor;
                      if(a == H.getPcolumnas5()[s][j]){
                      H.getPfilas5()[s+1][j].setBackground(Color.cyan);
                      cont6 ++;
                       
                       if(cont6 ==24){
                           Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 6");
                       }
                      }
                
            }

        }
                                 
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     
                     }else{
                         
                         if(objetoVista.getjCheckBox2().isSelected()){
                             if(x==1){
                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                     }else{
                         if(x==2){
                              for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }  
                           for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.pink);
                          cont2 ++;
                          if(cont2 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                         }else{
                             if(x==3){
                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.pink);
                          cont2 ++;
                          if(cont2 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.pink);
                          cont3 ++;
                          if(cont3 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                             }else{
                                 if(x==4){
                                     for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.pink);
                          cont2 ++;
                          if(cont2 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.pink);
                          cont3 ++;
                          if(cont3 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.pink);
                          cont4 ++;
                          if(cont4 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                 
                                 }else{
                                     if(x==5){
                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.pink);
                          cont2 ++;
                          if(cont2 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.pink);
                          cont3 ++;
                          if(cont3 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.pink);
                          cont4 ++;
                          if(cont4 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.pink);
                          cont5 ++;
                          if(cont5 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                     }else{
                                         if(x==6){
                                            for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.pink);
                          cont1 ++;
                          if(cont1 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.pink);
                          cont2 ++;
                          if(cont2 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.pink);
                          cont3 ++;
                          if(cont3 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.pink);
                          cont4 ++;
                          if(cont4 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.pink);
                          cont5 ++;
                          if(cont5 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                for(int s = 0; s < H.Pcolumnas5.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas5[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==0 || s==2 && j==0 || s==3 && j==0 || s==4 && j==0 || s==0 && j==1 || s==0 && j==2 || s==0 && j==3 || s==0 && j==4 || s==1 && j==4 || s==2 && j==4|| s==3 && j==4|| s==4 && j==4|| s==4 && j==3 || s==4 && j==2 || s==4 && j==1){
                            if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.pink);
                          cont6 ++;
                          if(cont6 == 16){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 6");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               
                                 
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     
                         }else{
                             if(objetoVista.getjCheckBox3().isSelected()){
                                 if(x==1){
                                     for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4 ){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                 }else{
                                     if(x==2){
                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }  
                           for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.red);
                          cont2 ++;
                          if(cont2 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                     }else{
                                         if(x==3){
                                             for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.red);
                          cont2 ++;
                          if(cont2 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.red);
                          cont3 ++;
                          if(cont3 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                         }else{
                                             if(x==4){
                                                 for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.red);
                          cont2 ++;
                          if(cont2 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.red);
                          cont3 ++;
                          if(cont3 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.red);
                          cont4 ++;
                          if(cont4 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                             }else{
                                                 if(x==5){
                                                      for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.red);
                          cont2 ++;
                          if(cont2 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.red);
                          cont3 ++;
                          if(cont3 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.red);
                          cont4 ++;
                          if(cont4 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.red);
                          cont5 ++;
                          if(cont5 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                                 }else{
                                                     if(x==6){
                                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.red);
                          cont1 ++;
                          if(cont1 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.red);
                          cont2 ++;
                          if(cont2 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.red);
                          cont3 ++;
                          if(cont3 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.red);
                          cont4 ++;
                          if(cont4 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.red);
                          cont5 ++;
                          if(cont5 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                for(int s = 0; s < H.Pcolumnas5.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas5[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==1 && j==1 || s==3 && j==3 || s==4 && j==4 || s==4 && j==0 || s==3 && j==1 || s==1 && j==3 || s==0 && j==4){
                            if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.red);
                          cont6 ++;
                          if(cont6 == 8){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 6");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }
                                 
                                 
                             }else{
                                 if(objetoVista.getjCheckBox4().isSelected()){
                                 if(x==1){
                                     for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                 }else{
                                     if(x==2){
                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }  
                           for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.MAGENTA);
                          cont2 ++;
                          if(cont2 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                     }else{
                                         if(x==3){
                                             for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.MAGENTA);
                          cont2 ++;
                          if(cont2 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.MAGENTA);
                          cont3 ++;
                          if(cont3 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                         }else{
                                             if(x==4){
                                                 for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.MAGENTA);
                          cont2 ++;
                          if(cont2 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.MAGENTA);
                          cont3 ++;
                          if(cont3 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.MAGENTA);
                          cont4 ++;
                          if(cont4 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                             }else{
                                                 if(x==5){
                                                      for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.MAGENTA);
                          cont2 ++;
                          if(cont2 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.MAGENTA);
                          cont3 ++;
                          if(cont3 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.MAGENTA);
                          cont4 ++;
                          if(cont4 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.MAGENTA);
                          cont5 ++;
                          if(cont5 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                                 }else{
                                                     if(x==6){
                                                         for(int s = 0; s < H.Pcolumnas.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.MAGENTA);
                          cont1 ++;
                          if(cont1 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 1");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas[s][j] ){
                          H.Pfilas[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              
                               for(int s = 0; s < H.Pcolumnas1.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas1[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.MAGENTA);
                          cont2 ++;
                          if(cont2 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 2");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas1[s][j] ){
                          H.Pfilas1[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                               for(int s = 0; s < H.Pcolumnas2.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas2[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.MAGENTA);
                          cont3 ++;
                          if(cont3 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 3");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas2[s][j] ){
                          H.Pfilas2[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas3.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas3[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.MAGENTA);
                          cont4 ++;
                          if(cont4 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 4");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas3[s][j] ){
                          H.Pfilas3[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                              for(int s = 0; s < H.Pcolumnas4.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas4[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.MAGENTA);
                          cont5 ++;
                          if(cont5 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 5");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas4[s][j] ){
                          H.Pfilas4[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                for(int s = 0; s < H.Pcolumnas5.length; s++)
                     {
                     for(int j = 0; j < H.Pcolumnas5[0].length; j++)
                     {  
                        int a = valor;
                        if(s==0 && j==0 || s==0 && j==4 || s==4 && j==0 || s==4 && j==4){
                            if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.MAGENTA);
                          cont6 ++;
                          if(cont6 == 4){
                          Ganador ga = new Ganador();
                          ga.setVisible(true);
                          ga.getJlGanador().setText("GANADOR CARTON NUMERO 6");
                       }
                      }
                        }else{
                              if(a == H.Pcolumnas5[s][j] ){
                          H.Pfilas5[s+1][j].setBackground(Color.cyan);
                              }
                          }
                      
                      
                      
                
            }

        }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }
                                 
                                 
                             }
                             }
                         }
                     }
                     
                     
                     
                     
                     
                      
                     
                      
                      
                      
               
                } else
                {
                    JOptionPane.showMessageDialog(null, "Sorteo Finalizado.");
                }
                
                   
            }
            
        }
                             
  ); 
            
            
            
            
            frame.setVisible(true);
            TODO.setVisible(true);
            T.setVisible(true);
            S.setVisible(true);
            
            
            
            
             
          }else{
              
              JOptionPane.showMessageDialog(null, "Debe elegir el modo de juego");
              
          }
            
        }
        
    }
    
    
    
    
   int valor;
   int cont1 = 0;
   int cont2 = 0;
   int cont3 = 0;
   int cont4 = 0;
   int cont5 = 0;
   int cont6 = 0;
    
}
    
